import React, { Component } from 'react'
import Swal from 'sweetalert2'
import 'animate.css';
import UserTableComponent from './UserTableComponent';

export class Information extends Component {
  constructor() {

    super();
    this.state = {
      userInfo: [],
    }
  }

  onAlert = (item) => {
    Swal.fire({
      html: '<b>' + 'ID : ' + item.id +
        '<br>Email : ' + item.email +
        '<br>Name : ' + item.name +
        '<br>Age : ' + item.age +
        '<br>Status : ' + (item.status ? "Done" : "Pending") +
        '</b>',
      showClass: {
        popup: 'animate__animated animate__fadeInRight'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutRight'
      }
    })
  }


  handleChangeEmail = (event) => {
    
    if(event.target.value === ""){
      this.setState({
        newUserEmail: "Null",
      })
    }else{
      this.setState({
        newUserEmail: event.target.value,
      })
    }

  }

  handleChangeName = (event) => {

    if(event.target.value === ""){
      this.setState({
        newUserName: "Null",
      })
    }else{
      this.setState({
        newUserName: event.target.value,
      })
    }
  }

  handleChangeAge = (event) => {
    if(event.target.value === ""){
      this.setState({
        newUserAge: "Null",
      })
    }else{
      this.setState({
        newUserAge: event.target.value,
      })
    }
    
  }

  handleUpdate = (obj) => {
    this.setState({
      userInfo: [...this.state.userInfo, obj],
    })
  }

  onRegister = () => {
    const obj = {
      id: this.state.userInfo.length + 1,
      email: this.state.newUserEmail ?? 'Null',
      name: this.state.newUserName ?? 'Null',
      age: this.state.newUserAge ?? 'Null',
      status: false,
    }
    this.setState({
      userInfo: [...this.state.userInfo, obj],
    },
    )
  }

  getItem = (data) => {
    let updatedList = this.state.userInfo.map(item => {
      if (item.id === data.id) {
        return { ...item, status: !data.status };
      }
      return item; 
    });
    this.setState({ userInfo: updatedList }); 
  }

  render() {
    return (
      <div className='w-8/12'>
        {/* start form  */}
        
        <h1 className=" mt-16 mb-16 text  text-3xl font-extrabold text-blue-500  md:text-5xl lg:text-6xl"><span className="text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600 ">PLEASE FILL YOUR </span> INFO</h1>
        
        <label htmlFor="email-address-icon" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your Email</label>
        <div className="relative">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
          </div>
          <input type="text" id="email" onChange={this.handleChangeEmail} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  " placeholder="Ex: david@gmail.com" />
        </div>


        <label htmlFor="website-admin" className=" mt-10 block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
        <div className="flex">
          <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            @
          </span>
          <input type="text" id="username" onChange={this.handleChangeName} className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 " placeholder="Ex: Ken David" />
        </div>

        <label htmlFor="website-admin" className="  block mt-10 mb-2 text-sm font-medium text-gray-900 dark:text-white">Age</label>
        <div className="flex">
          <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            ❤️
          </span>
          <input type="text" id="age" onChange={this.handleChangeAge} className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Ex: 20" />
        </div>
        {/* end form */}

        <div className='flex justify-center pt-10 pb-6 '>
          {/* start button register */}
          <button onClick={this.onRegister} className="w-60 relative inline-flex items-center justify-center p-0.5 mb-2 mr-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-purple-600 to-blue-500 group-hover:from-purple-600 group-hover:to-blue-500 hover:text-white  focus:ring-4 focus:outline-none focus:ring-blue-300 ">
            <span className=" w-60 relative px-5 py-2.5 transition-all ease-in duration-75 bg-white  rounded-md group-hover:bg-opacity-0">
              Register
            </span>
          </button>
          {/* end button register */}
        </div>
        <div className='mb-28'>
          <UserTableComponent data={this.state.userInfo} getItem={this.getItem} onAlert={this.onAlert} />
        </div>
      </div>
    )
  }
}

export default Information
