import React, { Component } from 'react'


export class UserTableComponent extends Component {

    render() {
        return (
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table className="w-full text-sm text-left text-gray-500 ">
                    <thead className="text-xs text-gray-700 uppercase bg-purple-300 ">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                ID
                            </th>
                            <th scope="col" className="px-6 py-3">
                                EMAIL
                            </th>
                            <th scope="col" className="px-6 py-3">
                                USERNAME
                            </th>
                            <th scope="col" className="px-6 py-3">
                                AGE
                            </th>
                            <th scope="col" className="px-6 py-3 flex justify-center">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item) => (
                            <tr key={item.id} className="bg-white border-b even:bg-blue-200 odd:bg-white ">
                                <td className="px-6 py-3">{item.id}</td>
                                <td className="px-6 py-3">{item.email}</td>   
                                <td className="px-6 py-3">{item.name}</td>
                                <td className="px-6 py-3">{item.age}</td>
                                <td className="px-6 py-3 flex justify-end">
                                    <button onClick={() => this.props.getItem(item)} type="button" className={item.status ? "w-40 flex-shrink-0 focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 "
                                        : " w-40 flex-shrink-0 focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 "}>{item.status ? 'Done' : 'Pending'}</button>
                                    <button onClick={() => this.props.onAlert(item)} type="button" className=" w-40 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2  focus:outline-none ">Show More</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default UserTableComponent